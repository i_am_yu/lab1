import math
def check(char=None):
    """
    使用说明：输入一个字符，判断是否合法
    返回值：若合法且为数字则返回2，若为计算符号则返回1，否则返回0
    """
    if str.isdigit(char):
        return 2
    elif iscal(char):
        return 1
    else:
        return 0

def iscal(char=None):
    """
    判断是否为合法的运算符
    返回值：若是，则返回True，否则返回False
    """
    if char == "+" or char == "-" or char == "^"or char == "%" or char == "/" or char == "sin" or char == "cos" or char == "tan" or char == "*":
        return True
    else:
        return False

def terminate(count,num):
    if count % 2 == 1:
        print("输入end结束，输入其他字符则继续：")
        sign = input()
        if sign == "end":
            print("最终结果为：", num)
            exit(0)


char1 = None
print("""
使用说明：输入'+'代表加号，输入'-'代表减号，输入'*'代表乘号，输入'/'代表除号，输入'%'代表求余，输入'^'代表乘方
输入'sin'代表求正弦值，'cos','tan'同理，输入'end'退出
计算三角函数时，函数值取决于下一次输入的数字
""")
count = 1
print("请输入一个初始化值：")
num = float(input())
print("初始化成功：num=",num)
while True:
    if count % 2 == 1 and count != 1:
        print("当前结果为：",num)
        terminate(count, num)
    if count % 2 == 1:                                             # 输入提示
        print("请输入一个运算符:")
    else:
        print("请输入一个数字：")

    char = input()

    if check(char) == 0:                                           # 对字符进行检查
        print("输入了非法字符！请重新输入！")
        continue
    elif check(char) == 2:                                       # 计算部分
        count += 1
        if char1 == '+':
            num+=float(char)
        if char1 == '-':
            num-=float(char)
        if char1 == '/':
            num/=float(char)
        if char1 == '%':
            num%=int(char)
        if char1 == '*':
            num*=float(char)
        if char1 == "sin":
            num=math.sin(float(char))
        if char1 == "cos":
            num = math.cos(float(char))
        if char1 == "tan":
            num = math.tan(float(char))
        if char1 == "^":
            num = pow(num,float(char))


    else:
        count += 1                                                  # 保存计算符号
        char1 = char
        print("输入成功，输入的符号是："+char1)
